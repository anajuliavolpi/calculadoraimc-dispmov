package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText edPeso;
    EditText edAltura;
    ImageView imagem;
    TextView resultadoValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edPeso = findViewById(R.id.edPeso);
        edAltura = findViewById(R.id.edAltura);
        imagem = findViewById(R.id.imagem);
        resultadoValor = findViewById(R.id.resultadoValor);
    }

    public void calcular(View view){

        double peso = Double.parseDouble(edPeso.getText().toString());
        double altura = Double.parseDouble(edAltura.getText().toString());
        double imc = peso/(altura*altura);

        if (imc < 18.5){
            imagem.setImageResource(R.drawable.abaixopeso);
        }
        else if (imc < 24.99){
            imagem.setImageResource(R.drawable.normal);
        }
        else if (imc < 29.99){
            imagem.setImageResource(R.drawable.sobrepeso);
        }
        else if(imc < 34.99){
            imagem.setImageResource(R.drawable.obesidade1);
        }
        else if(imc < 39.99){
            imagem.setImageResource(R.drawable.obesidade2);
        }
        else {
            imagem.setImageResource(R.drawable.obesidade3);
        }

        DecimalFormat formato = new DecimalFormat("#.##");

        //this.resultadoValor.setText(Double.toString(Math.round(imc)));

        String imcResultado = formato.format(imc);
        this.resultadoValor.setText(imcResultado);
    }

}
